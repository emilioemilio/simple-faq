<?php
/**
 * @file
 * sfaq.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function sfaq_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'sfaq';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'field_collection_item';
  $view->human_name = 'sFAQ';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'más';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Aplicar';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reiniciar';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Ordenar por';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['type'] = 'ol';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Relationship: Field collection item: Entity with the FAQ item (field_faq_item) */
  $handler->display->display_options['relationships']['field_faq_item_node_1']['id'] = 'field_faq_item_node_1';
  $handler->display->display_options['relationships']['field_faq_item_node_1']['table'] = 'field_collection_item';
  $handler->display->display_options['relationships']['field_faq_item_node_1']['field'] = 'field_faq_item_node';
  $handler->display->display_options['relationships']['field_faq_item_node_1']['required'] = TRUE;
  /* Field: Field collection item: Field collection item ID */
  $handler->display->display_options['fields']['item_id']['id'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['table'] = 'field_collection_item';
  $handler->display->display_options['fields']['item_id']['field'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['label'] = '';
  $handler->display->display_options['fields']['item_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['item_id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['item_id']['element_default_classes'] = FALSE;
  /* Field: Field collection item: Pregunta */
  $handler->display->display_options['fields']['field_pregunta']['id'] = 'field_pregunta';
  $handler->display->display_options['fields']['field_pregunta']['table'] = 'field_data_field_pregunta';
  $handler->display->display_options['fields']['field_pregunta']['field'] = 'field_pregunta';
  $handler->display->display_options['fields']['field_pregunta']['label'] = '';
  $handler->display->display_options['fields']['field_pregunta']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_pregunta']['alter']['text'] = '<a href="#[item_id]">[field_pregunta-value]</a>';
  $handler->display->display_options['fields']['field_pregunta']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_pregunta']['element_default_classes'] = FALSE;
  /* Sort criterion: Content: FAQ item (field_faq_item:delta) */
  $handler->display->display_options['sorts']['delta']['id'] = 'delta';
  $handler->display->display_options['sorts']['delta']['table'] = 'field_data_field_faq_item';
  $handler->display->display_options['sorts']['delta']['field'] = 'delta';
  $handler->display->display_options['sorts']['delta']['relationship'] = 'field_faq_item_node_1';
  /* Contextual filter: Content: FAQ item (field_faq_item) */
  $handler->display->display_options['arguments']['field_faq_item_value']['id'] = 'field_faq_item_value';
  $handler->display->display_options['arguments']['field_faq_item_value']['table'] = 'field_data_field_faq_item';
  $handler->display->display_options['arguments']['field_faq_item_value']['field'] = 'field_faq_item_value';
  $handler->display->display_options['arguments']['field_faq_item_value']['relationship'] = 'field_faq_item_node_1';
  $handler->display->display_options['arguments']['field_faq_item_value']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_faq_item_value']['exception']['title'] = 'Todo(s)';
  $handler->display->display_options['arguments']['field_faq_item_value']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['field_faq_item_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_faq_item_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_faq_item_value']['summary_options']['items_per_page'] = '25';

  /* Display: Preg y resp Colapsables */
  $handler = $view->new_display('block', 'Preg y resp Colapsables', 'pregyrespcoll');
  $handler->display->display_options['display_description'] = 'para method 1';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class'] = 'sfaq';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['type'] = 'ol';
  $handler->display->display_options['style_options']['class'] = 'sfaq';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Field collection item: Pregunta */
  $handler->display->display_options['fields']['field_pregunta']['id'] = 'field_pregunta';
  $handler->display->display_options['fields']['field_pregunta']['table'] = 'field_data_field_pregunta';
  $handler->display->display_options['fields']['field_pregunta']['field'] = 'field_pregunta';
  $handler->display->display_options['fields']['field_pregunta']['label'] = '';
  $handler->display->display_options['fields']['field_pregunta']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_pregunta']['alter']['text'] = '<label for="[nothing_2]-[counter]">[field_pregunta]</label>';
  $handler->display->display_options['fields']['field_pregunta']['element_label_colon'] = FALSE;
  /* Field: class-name */
  $handler->display->display_options['fields']['nothing_2']['id'] = 'nothing_2';
  $handler->display->display_options['fields']['nothing_2']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_2']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_2']['ui_name'] = 'class-name';
  $handler->display->display_options['fields']['nothing_2']['label'] = '';
  $handler->display->display_options['fields']['nothing_2']['alter']['text'] = 'sfaq';
  $handler->display->display_options['fields']['nothing_2']['element_label_colon'] = FALSE;
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '';
  $handler->display->display_options['fields']['counter']['alter']['text'] = '<input type="checkbox" id="[nothing_2]-[counter]">';
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['counter']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Field: Field collection item: Field collection item ID */
  $handler->display->display_options['fields']['item_id']['id'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['table'] = 'field_collection_item';
  $handler->display->display_options['fields']['item_id']['field'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['label'] = '';
  $handler->display->display_options['fields']['item_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['item_id']['element_label_colon'] = FALSE;
  /* Field: Field collection item: Respuesta */
  $handler->display->display_options['fields']['field_respuesta']['id'] = 'field_respuesta';
  $handler->display->display_options['fields']['field_respuesta']['table'] = 'field_data_field_respuesta';
  $handler->display->display_options['fields']['field_respuesta']['field'] = 'field_respuesta';
  $handler->display->display_options['fields']['field_respuesta']['label'] = '';
  $handler->display->display_options['fields']['field_respuesta']['alter']['text'] = '<div class="[nothing_2]">[field_respuesta]</div>';
  $handler->display->display_options['fields']['field_respuesta']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_respuesta']['element_default_classes'] = FALSE;
  $handler->display->display_options['block_description'] = 'Preguntas y respuestas colapsables';

  /* Display: PyR collaps m1 */
  $handler = $view->new_display('block', 'PyR collaps m1', 'sfaq_pregyresp_coll');
  $handler->display->display_options['display_description'] = 'para method 1';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['type'] = 'ol';
  $handler->display->display_options['style_options']['class'] = 'sfaq';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: class-name */
  $handler->display->display_options['fields']['nothing_2']['id'] = 'nothing_2';
  $handler->display->display_options['fields']['nothing_2']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_2']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_2']['ui_name'] = 'class-name';
  $handler->display->display_options['fields']['nothing_2']['label'] = '';
  $handler->display->display_options['fields']['nothing_2']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nothing_2']['alter']['text'] = 'sfaq';
  $handler->display->display_options['fields']['nothing_2']['element_label_colon'] = FALSE;
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['exclude'] = TRUE;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Field: Field collection item: Field collection item ID */
  $handler->display->display_options['fields']['item_id']['id'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['table'] = 'field_collection_item';
  $handler->display->display_options['fields']['item_id']['field'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['label'] = '';
  $handler->display->display_options['fields']['item_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['item_id']['element_label_colon'] = FALSE;
  /* Field: Field collection item: Pregunta */
  $handler->display->display_options['fields']['field_pregunta']['id'] = 'field_pregunta';
  $handler->display->display_options['fields']['field_pregunta']['table'] = 'field_data_field_pregunta';
  $handler->display->display_options['fields']['field_pregunta']['field'] = 'field_pregunta';
  $handler->display->display_options['fields']['field_pregunta']['label'] = '';
  $handler->display->display_options['fields']['field_pregunta']['alter']['text'] = '<a class="[nothing_2]-open" href="#open-[counter]" id="open-[counter]"  tabindex="[counter]">[field_pregunta]</a>';
  $handler->display->display_options['fields']['field_pregunta']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_pregunta']['element_default_classes'] = FALSE;
  /* Field: open-link */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['ui_name'] = 'open-link';
  $handler->display->display_options['fields']['nothing_1']['label'] = '';
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = '<a class="[nothing_2]-open" href="#open-[counter]" id="open-[counter]" tabindex="[counter]">+</a>';
  $handler->display->display_options['fields']['nothing_1']['element_label_colon'] = FALSE;
  /* Field: close-link */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'close-link';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a class="[nothing_2]-close" href="#close-[counter]" id="close-[counter]" tabindex="[counter]">-</a>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Field: Field collection item: Respuesta */
  $handler->display->display_options['fields']['field_respuesta']['id'] = 'field_respuesta';
  $handler->display->display_options['fields']['field_respuesta']['table'] = 'field_data_field_respuesta';
  $handler->display->display_options['fields']['field_respuesta']['field'] = 'field_respuesta';
  $handler->display->display_options['fields']['field_respuesta']['label'] = '';
  $handler->display->display_options['fields']['field_respuesta']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_respuesta']['alter']['text'] = '<div class="[nothing_2]">[field_respuesta]</div>';
  $handler->display->display_options['fields']['field_respuesta']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_respuesta']['element_default_classes'] = FALSE;
  $handler->display->display_options['block_description'] = 'Preguntas y respuestas colapsables m1';

  /* Display: Preguntas */
  $handler = $view->new_display('attachment', 'Preguntas', 'sfaq_preguntas');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['displays'] = array(
    'sfaq_preguntasyrespuestas' => 'sfaq_preguntasyrespuestas',
    'default' => 0,
    'block_1' => 0,
  );

  /* Display: Preguntas y Respuestas */
  $handler = $view->new_display('block', 'Preguntas y Respuestas', 'sfaq_preguntasyrespuestas');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['type'] = 'ol';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Field collection item: Field collection item ID */
  $handler->display->display_options['fields']['item_id']['id'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['table'] = 'field_collection_item';
  $handler->display->display_options['fields']['item_id']['field'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['label'] = '';
  $handler->display->display_options['fields']['item_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['item_id']['element_label_colon'] = FALSE;
  /* Field: Field collection item: Pregunta */
  $handler->display->display_options['fields']['field_pregunta']['id'] = 'field_pregunta';
  $handler->display->display_options['fields']['field_pregunta']['table'] = 'field_data_field_pregunta';
  $handler->display->display_options['fields']['field_pregunta']['field'] = 'field_pregunta';
  $handler->display->display_options['fields']['field_pregunta']['label'] = '';
  $handler->display->display_options['fields']['field_pregunta']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_pregunta']['alter']['text'] = '<a id="[item_id]">[field_pregunta]</a>';
  $handler->display->display_options['fields']['field_pregunta']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_pregunta']['element_default_classes'] = FALSE;
  /* Field: Field collection item: Respuesta */
  $handler->display->display_options['fields']['field_respuesta']['id'] = 'field_respuesta';
  $handler->display->display_options['fields']['field_respuesta']['table'] = 'field_data_field_respuesta';
  $handler->display->display_options['fields']['field_respuesta']['field'] = 'field_respuesta';
  $handler->display->display_options['fields']['field_respuesta']['label'] = '';
  $handler->display->display_options['fields']['field_respuesta']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_respuesta']['element_default_classes'] = FALSE;
  $translatables['sfaq'] = array(
    t('Master'),
    t('más'),
    t('Aplicar'),
    t('Reiniciar'),
    t('Ordenar por'),
    t('Asc'),
    t('Desc'),
    t('field_faq_item'),
    t('.'),
    t(','),
    t('<a href="#[item_id]">[field_pregunta-value]</a>'),
    t('Todo(s)'),
    t('Preg y resp Colapsables'),
    t('para method 1'),
    t('more'),
    t('<label for="[nothing_2]-[counter]">[field_pregunta]</label>'),
    t('sfaq'),
    t('<input type="checkbox" id="[nothing_2]-[counter]">'),
    t('<div class="[nothing_2]">[field_respuesta]</div>'),
    t('Preguntas y respuestas colapsables'),
    t('PyR collaps m1'),
    t('View result counter'),
    t('<a class="[nothing_2]-open" href="#open-[counter]" id="open-[counter]"  tabindex="[counter]">[field_pregunta]</a>'),
    t('<a class="[nothing_2]-open" href="#open-[counter]" id="open-[counter]" tabindex="[counter]">+</a>'),
    t('<a class="[nothing_2]-close" href="#close-[counter]" id="close-[counter]" tabindex="[counter]">-</a>'),
    t('Preguntas y respuestas colapsables m1'),
    t('Preguntas'),
    t('Preguntas y Respuestas'),
    t('<a id="[item_id]">[field_pregunta]</a>'),
  );
  $export['sfaq'] = $view;

  return $export;
}
